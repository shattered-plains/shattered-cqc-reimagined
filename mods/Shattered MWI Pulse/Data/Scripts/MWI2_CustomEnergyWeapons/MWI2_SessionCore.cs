using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sandbox.Common;
using Sandbox.Common.ObjectBuilders;
using Sandbox.Common.ObjectBuilders.Definitions;
using Sandbox.Definitions;
using Sandbox.Game;
using Sandbox.Game.Entities;
using Sandbox.Game.EntityComponents;
using Sandbox.Game.GameSystems;
using Sandbox.ModAPI;
using Sandbox.ModAPI.Interfaces;
using Sandbox.ModAPI.Interfaces.Terminal;
using SpaceEngineers.Game.ModAPI;
using VRage.Game;
using VRage.Game.Components;
using VRage.Game.Entity;
using VRage.Game.ModAPI;
using VRage.ModAPI;
using VRage.ObjectBuilders;
using VRage.Utils;
using VRageMath;

namespace MWI2_CustomEnergyWeapons{
	
	[MySessionComponentDescriptor(MyUpdateOrder.BeforeSimulation)]
	
	public class MWI2_SessionCore : MySessionComponentBase {
        //Configuration
        public static Guid MyModStorageGuid = new Guid("DE3ED11E-441D-490F-9849-9527564C8C7D"); //Change This To A Unique GUID For Your Mod
		
		/*
		
		For Color References, Please Visit Link Below:
		
		https://github.com/KeenSoftwareHouse/SpaceEngineers/blob/a109106fc0ded66bdd5da70e099646203c56550f/Sources/VRage.Math/Color.cs
		
		*/
		
		//Do Not Edit Values Below Here
		
		public static bool SetupComplete = false;
		public int tickTimer = 0;

        // DS Added - ShieldApi Support
        internal static MWI2_SessionCore Instance { get; private set; } // allow access from gamelogic
        public bool ShieldMod { get; set; }
        public bool ShieldApiLoaded { get; set; }
        public ShieldApi SApi = new ShieldApi();
        public override void UpdateBeforeSimulation(){
            // Ds Added - If Shield in world and API not Loaded see if Shield APi is ready and if so load it.
            if (ShieldMod && !ShieldApiLoaded && SApi.Load())
                ShieldApiLoaded = true;
        }

        // Ds Added - Load and Unload Shield Api
        public override void LoadData() {
            Instance = this;
            foreach (var mod in MyAPIGateway.Session.Mods)
                if (mod.PublishedFileId == 1365616918) ShieldMod = true;
        }
        protected override void UnloadData(){
            if (ShieldApiLoaded) SApi.Unload();
            Instance = null;
        }
		
	}
	
}