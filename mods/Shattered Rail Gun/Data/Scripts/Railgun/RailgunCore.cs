﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sandbox.ModAPI;
using VRage.Collections;
using VRage.Game;
using VRage.Game.Components;
using Rexxar;
using Rexxar.Communication;
using Whiplash.ArmorPiercingProjectiles;
using VRage.ModAPI;
using Sandbox.Game.Entities;
using VRageMath;
using VRage.Utils;
using VRage.Game.ModAPI.Ingame.Utilities;

namespace Whiplash.Railgun
{
    [MySessionComponentDescriptor(MyUpdateOrder.AfterSimulation | MyUpdateOrder.BeforeSimulation)]
    public class RailgunCore : MySessionComponentBase
    {
        #region Member Fields
        public static bool IsServer;
        int _count;
        static List<ArmorPiercingProjectile> liveProjectiles = new List<ArmorPiercingProjectile>();
        static HashSet<MyPlanet> _planets = new HashSet<MyPlanet>();
        readonly MyIni myIni = new MyIni();
        static List<ArmorPiercingProjectileClient> _clientProjectiles = new List<ArmorPiercingProjectileClient>();
        public static bool SessionInit { get; private set; } = false;

        // Shield Api
        internal static RailgunCore Instance { get; private set; } // DS - Allow access from gamelogic
        public bool ShieldMod { get; set; }
        public bool ShieldApiLoaded { get; set; }
        public ShieldApi ShieldApi = new ShieldApi();
        #endregion

        #region Update and Init

        public override void BeforeStart()
        {
            base.BeforeStart();
            IsServer = MyAPIGateway.Multiplayer.IsServer || MyAPIGateway.Session.OnlineMode == MyOnlineModeEnum.OFFLINE;

            if (IsServer)
                LoadConfig(ref RailgunConstants.Config);

            foreach (var mod in MyAPIGateway.Session.Mods)
                if (mod.PublishedFileId == 1365616918) ShieldMod = true; //DS - detect shield is installed
        }

        public override void UpdateAfterSimulation()
        {
            base.UpdateAfterSimulation();

            if (!SessionInit)
            {
                SessionInit = true;
                
                Communication.Register();

                if (++_count % 10 == 0)
                    Settings.SyncSettings();
            }


        }

        public override void UpdateBeforeSimulation()
        {
            base.UpdateBeforeSimulation();

            if (ShieldMod && !ShieldApiLoaded && ShieldApi.Load()) // DS - Init API.
                ShieldApiLoaded = true;

            if (IsServer)
                SimulateProjectiles();
        }
        #endregion

        #region Projectile Methods
        private static void SimulateProjectiles()
        {
            //projectile simulation
            for (int i = liveProjectiles.Count - 1; i >= 0; i--)
            {
                var projectile = liveProjectiles[i];

                projectile.Update();

                if (projectile.ShouldDrawTracer)
                {
                    RailgunTracerData tracer = projectile.CurrentTracer;
                    RailgunTracerSync.SendToClients(tracer);
                }

                if (projectile.Killed)
                    liveProjectiles.RemoveAt(i);
            }
        }

        public static void ShootProjectileServer(RailgunFireData fireData)
        {
            var projectile = new ArmorPiercingProjectile(fireData);
            AddProjectile(projectile);
        }

        public static void DrawProjectileClient(RailgunTracerData tracerData)
        {
            var projectile = new ArmorPiercingProjectileClient(tracerData);
            _clientProjectiles.Add(projectile);
        }

        readonly List<ArmorPiercingProjectileClient> _removeList = new List<ArmorPiercingProjectileClient>();
        public override void Draw()
        {
            base.Draw();

            _removeList.Clear();
            foreach (var projectile in _clientProjectiles)
            {
                projectile.DrawTracer();

                if (projectile.Remove)
                    _removeList.Add(projectile);
            }

            foreach (var projectile in _removeList)
            {
                _clientProjectiles.Remove(projectile);
            }
        }

        public static void AddProjectile(ArmorPiercingProjectile projectile)
        {
            liveProjectiles.Add(projectile);
        }
        #endregion


        #region Planets and Gravity
        private void AddPlanet(IMyEntity entity)
        {
            var planet = entity as MyPlanet;
            if (planet != null)
                _planets.Add(planet);
        }

        private void RemovePlanet(IMyEntity entity)
        {
            var planet = entity as MyPlanet;
            if (planet != null)
                _planets.Remove(planet);
        }

        public static Vector3D GetNaturalGravityAtPoint(Vector3D point)
        {
            var gravity = Vector3D.Zero;
            foreach (var planet in _planets)
            {
                IMyGravityProvider gravityProvider = planet.Components.Get<MyGravityProviderComponent>();
                if (gravityProvider != null)
                    gravity += gravityProvider.GetWorldGravity(point);
            }
            return gravity;
        }
        #endregion

        #region Load and Unload Data
        public override void LoadData()
        {
            Instance = this; // DS - assign Session instance.

            MyAPIGateway.Entities.OnEntityAdd += AddPlanet;
            MyAPIGateway.Entities.OnEntityRemove += RemovePlanet;

            base.LoadData();
        }

        protected override void UnloadData()
        {
            base.UnloadData();
            Communication.Unregister();
            if (ShieldApiLoaded) ShieldApi.Unload(); // DS - unload api
            Instance = null; // DS - null Instance method.
            MyAPIGateway.Entities.OnEntityAdd -= AddPlanet;
            MyAPIGateway.Entities.OnEntityRemove -= RemovePlanet;
        }
        #endregion

        #region Config Save and Load       
        void LoadConfig(ref RailgunConfig config)
        {
            if (!MyAPIGateway.Utilities.FileExistsInLocalStorage(RailgunConstants.CONFIG_FILE_NAME, typeof(RailgunCore)))
            {
                MyAPIGateway.Utilities.ShowMessage(RailgunConstants.DEBUG_MSG_TAG, $"{RailgunConstants.CONFIG_FILE_NAME} not found. Writing defaults...");
                SaveConfig(ref RailgunConstants.Config);
                return;
            }

            string settings;
            using (var Reader = MyAPIGateway.Utilities.ReadFileInLocalStorage(RailgunConstants.CONFIG_FILE_NAME, typeof(RailgunCore)))
            {
                 settings = Reader.ReadToEnd();
            }

            myIni.Clear();
            bool parsed = myIni.TryParse(settings);
            if (!parsed)
            {
                MyAPIGateway.Utilities.ShowMessage(RailgunConstants.DEBUG_MSG_TAG, $"Config could not be parsed. Writing defaults...");
                SaveConfig(ref config);
                return;
            }

            config.ArtificialGravityMultiplier =   myIni.Get(RailgunConstants.INI_SECTION_TAG, RailgunConstants.INI_KEY_ART_GRAV).ToSingle(config.ArtificialGravityMultiplier);
            config.NaturalGravityMultiplier =      myIni.Get(RailgunConstants.INI_SECTION_TAG, RailgunConstants.INI_KEY_NAT_GRAV).ToSingle(config.NaturalGravityMultiplier);
            config.ShouldDrawProjectileTrails =    myIni.Get(RailgunConstants.INI_SECTION_TAG, RailgunConstants.INI_KEY_DRAW_TRAILS).ToBoolean(config.ShouldDrawProjectileTrails);
            config.PenetrationDamage =             myIni.Get(RailgunConstants.INI_SECTION_TAG, RailgunConstants.INI_KEY_PEN_DMG).ToSingle(config.PenetrationDamage);
            config.ExplosionRadius =               myIni.Get(RailgunConstants.INI_SECTION_TAG, RailgunConstants.INI_KEY_EXP_RAD).ToSingle(config.ExplosionRadius);
            config.ExplosionDamage =               myIni.Get(RailgunConstants.INI_SECTION_TAG, RailgunConstants.INI_KEY_EXP_DMG).ToSingle(config.ExplosionDamage);
            config.ShouldExplode =                 myIni.Get(RailgunConstants.INI_SECTION_TAG, RailgunConstants.INI_KEY_SHOULD_EXP).ToBoolean(config.ShouldExplode);
            config.ShouldPenetrate =               myIni.Get(RailgunConstants.INI_SECTION_TAG, RailgunConstants.INI_KEY_PEN).ToBoolean(config.ShouldPenetrate);
            config.PenetrationRange =              myIni.Get(RailgunConstants.INI_SECTION_TAG, RailgunConstants.INI_KEY_PEN_RANGE).ToSingle(config.PenetrationRange);
            config.IdlePowerDrawFixed =            myIni.Get(RailgunConstants.INI_SECTION_TAG, RailgunConstants.INI_KEY_PWR_FIXED).ToSingle(config.IdlePowerDrawFixed);
            config.IdlePowerDrawTurret =           myIni.Get(RailgunConstants.INI_SECTION_TAG, RailgunConstants.INI_KEY_PWR_TURRET).ToSingle(config.IdlePowerDrawTurret);
            config.ReloadPowerDraw =               myIni.Get(RailgunConstants.INI_SECTION_TAG, RailgunConstants.INI_KEY_PWR_RELOAD).ToSingle(config.ReloadPowerDraw);
            config.MuzzleVelocity =                myIni.Get(RailgunConstants.INI_SECTION_TAG, RailgunConstants.INI_KEY_MUZZLE_VEL).ToSingle(config.MuzzleVelocity);
            config.MaxRange =                      myIni.Get(RailgunConstants.INI_SECTION_TAG, RailgunConstants.INI_KEY_MAX_RANGE).ToSingle(config.MaxRange);
            config.DeviationAngleDeg =             myIni.Get(RailgunConstants.INI_SECTION_TAG, RailgunConstants.INI_KEY_DEVIANCE).ToSingle(config.DeviationAngleDeg);
            config.ProjectileTracerScale =         myIni.Get(RailgunConstants.INI_SECTION_TAG, RailgunConstants.INI_KEY_TRACER_SCALE).ToSingle(config.ProjectileTracerScale);
            config.RecoilImpulse =                 myIni.Get(RailgunConstants.INI_SECTION_TAG, RailgunConstants.INI_KEY_RECOIL).ToSingle(config.RecoilImpulse);
            config.ShieldDamageMultiplier =        myIni.Get(RailgunConstants.INI_SECTION_TAG, RailgunConstants.INI_KEY_SHIELD_MULT).ToSingle(config.ShieldDamageMultiplier);
            config.RateOfFireRPM =                 myIni.Get(RailgunConstants.INI_SECTION_TAG, RailgunConstants.INI_KEY_ROF).ToSingle(config.RateOfFireRPM);

            MyAPIGateway.Utilities.ShowMessage(RailgunConstants.DEBUG_MSG_TAG, $"Config loaded from:\n%AppData%\\SpaceEngineers\\Storage\\{ModContext.ModId}.sbm");

            SaveConfig(ref config, false, settings);

            if (RailgunConstants.Config.MaxRange > MyAPIGateway.Session.SessionSettings.SyncDistance)
            { 
                MyAPIGateway.Utilities.ShowMessage(RailgunConstants.DEBUG_MSG_TAG,
                        $"Sync distance is too low. Increasing from {MyAPIGateway.Session.SessionSettings.SyncDistance} to {RailgunConstants.Config.MaxRange}");
                MyAPIGateway.Session.SessionSettings.SyncDistance = (int)RailgunConstants.Config.MaxRange;
            }

            if (RailgunConstants.Config.MaxRange > MyAPIGateway.Session.SessionSettings.ViewDistance)
            {
                MyAPIGateway.Utilities.ShowMessage(RailgunConstants.DEBUG_MSG_TAG,
                        $"View distance is too low. Increasing from {MyAPIGateway.Session.SessionSettings.ViewDistance} to {RailgunConstants.Config.MaxRange}");
                MyAPIGateway.Session.SessionSettings.ViewDistance = (int)RailgunConstants.Config.MaxRange;
            }

        }

        void SaveConfig(ref RailgunConfig config, bool verbose = true, string settings = "")
        {
            myIni.Clear();
            myIni.Set(RailgunConstants.INI_SECTION_TAG, RailgunConstants.INI_KEY_ART_GRAV, config.ArtificialGravityMultiplier);
            myIni.Set(RailgunConstants.INI_SECTION_TAG, RailgunConstants.INI_KEY_NAT_GRAV, config.NaturalGravityMultiplier);
            myIni.Set(RailgunConstants.INI_SECTION_TAG, RailgunConstants.INI_KEY_DRAW_TRAILS, config.ShouldDrawProjectileTrails);
            myIni.Set(RailgunConstants.INI_SECTION_TAG, RailgunConstants.INI_KEY_SHOULD_EXP, config.ShouldExplode);
            myIni.Set(RailgunConstants.INI_SECTION_TAG, RailgunConstants.INI_KEY_EXP_RAD, config.ExplosionRadius);
            myIni.Set(RailgunConstants.INI_SECTION_TAG, RailgunConstants.INI_KEY_EXP_DMG, config.ExplosionDamage);
            myIni.Set(RailgunConstants.INI_SECTION_TAG, RailgunConstants.INI_KEY_PEN, config.ShouldPenetrate);
            myIni.Set(RailgunConstants.INI_SECTION_TAG, RailgunConstants.INI_KEY_PEN_DMG, config.PenetrationDamage);
            myIni.Set(RailgunConstants.INI_SECTION_TAG, RailgunConstants.INI_KEY_PEN_RANGE, config.PenetrationRange);
            myIni.Set(RailgunConstants.INI_SECTION_TAG, RailgunConstants.INI_KEY_PWR_FIXED, config.IdlePowerDrawFixed);
            myIni.Set(RailgunConstants.INI_SECTION_TAG, RailgunConstants.INI_KEY_PWR_TURRET, config.IdlePowerDrawTurret);
            myIni.Set(RailgunConstants.INI_SECTION_TAG, RailgunConstants.INI_KEY_PWR_RELOAD, config.ReloadPowerDraw);
            myIni.Set(RailgunConstants.INI_SECTION_TAG, RailgunConstants.INI_KEY_MUZZLE_VEL, config.MuzzleVelocity);
            myIni.Set(RailgunConstants.INI_SECTION_TAG, RailgunConstants.INI_KEY_MAX_RANGE, config.MaxRange);
            myIni.Set(RailgunConstants.INI_SECTION_TAG, RailgunConstants.INI_KEY_DEVIANCE, config.DeviationAngleDeg);
            myIni.Set(RailgunConstants.INI_SECTION_TAG, RailgunConstants.INI_KEY_TRACER_SCALE, config.ProjectileTracerScale);
            myIni.Set(RailgunConstants.INI_SECTION_TAG, RailgunConstants.INI_KEY_RECOIL, config.RecoilImpulse);
            myIni.Set(RailgunConstants.INI_SECTION_TAG, RailgunConstants.INI_KEY_SHIELD_MULT, config.ShieldDamageMultiplier);
            myIni.Set(RailgunConstants.INI_SECTION_TAG, RailgunConstants.INI_KEY_ROF, config.RateOfFireRPM);

            string finalOutput = myIni.ToString();

            if (finalOutput != settings)
            {
                using (var Writer = MyAPIGateway.Utilities.WriteFileInLocalStorage(RailgunConstants.CONFIG_FILE_NAME, typeof(RailgunCore)))
                    Writer.Write(finalOutput);

                if (verbose)
                    MyAPIGateway.Utilities.ShowMessage(RailgunConstants.DEBUG_MSG_TAG, $"Config saved to:\n%AppData%\\SpaceEngineers\\Storage\\{ModContext.ModId}.sbm");
            }
        }
        #endregion
    }
}
